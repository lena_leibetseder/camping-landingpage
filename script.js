document.addEventListener("DOMContentLoaded", function(event) {
    let matched = window.matchMedia('(prefers-color-scheme: dark)').matches;
    if(matched){
        document.body.classList.toggle('darkmode');
        document.getElementById("checkbox").checked = true;
    }


    const checkbox = document.getElementById('checkbox');

    checkbox.addEventListener('change', ()=>{
        document.body.classList.toggle('darkmode');
    })


    $('#recipeCarousel').carousel({
        interval: 10000
    })

    $('.carousel .carousel-item').each(function(){
        var minPerSlide = 3;
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i=0;i<minPerSlide;i++) {
            next=next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
        }
    });

});